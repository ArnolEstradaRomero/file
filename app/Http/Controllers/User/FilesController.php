<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class FilesController extends Controller
{
    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $max_size = (int)ini_get('upload_max_filesize') * 1024;
        $files = $request->file('files');
        $user_id = Auth::id();

        if($request->hasFile('files')){
            foreach ($files as $file) {
                  if (Storage::putFileAs('/public/' . $user_id . '/', $file,  $file->getClientOriginalName())){                      File::create([
                            'name'=> $file->getClientOriginalName(),
                            'user_id' => $user_id
                      ]);
                    }
            }
              Alert::success('Exito', 'Se ha subido el archivo');
              return back();
        }     else {
              Alert::error('Error', 'Deves subir un archivo');
              return back();
        }
      }
    }
